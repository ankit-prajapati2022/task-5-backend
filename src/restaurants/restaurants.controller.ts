import {
  Body,
  Controller,
  Param,
  Get,
  Post,
  Put,
  Delete,
} from '@nestjs/common';
import { RestaurantsService } from './restaurants.service';
import { Restaurants } from '../entity/restaurants.entity';
import { Comments } from 'src/entity/comments.entity';

// Get http://localhost:8000/restaurants
// Get http://localhost:8000/restaurants/${id}
// Post http://localhost:8000/restaurants
// Put http://localhost:8000/restaurants/${id}
// Delete http://localhost:8000/restaurants/${id}
// Get http://localhost:8000/restaurants/${id}/comments

@Controller('restaurants')
export class RestaurantsController {
  constructor(private readonly restaurantsService: RestaurantsService) {}

  @Get()
  findAll(): Promise<Restaurants[]> {
    return this.restaurantsService.findAll();
  }

  @Get(':id')
  findOne(@Param('id') id: number): Promise<Restaurants> {
    return this.restaurantsService.findOne(id);
  }

  @Post()
  create(
    @Body('name') name: string,
    @Body('location') location: string,
    @Body('avgRating') avgRating: number,
  ): Promise<Restaurants> {
    return this.restaurantsService.create(name, location, avgRating);
  }

  @Put(':id')
  update(
    @Param('id') id: number,
    @Body('name') name: string,
    @Body('location') location: string,
    @Body('avgRating') avgRating: number,
  ): Promise<Restaurants> {
    return this.restaurantsService.update(id, name, location, avgRating);
  }

  @Delete(':id')
  delete(@Param('id') id: number): Promise<Restaurants> {
    return this.restaurantsService.delete(id);
  }

  @Get(':id/comments')
  findComments(@Param('id') id: number): Promise<Comments[]> {
    return this.restaurantsService.findComments(id);
  }
}
