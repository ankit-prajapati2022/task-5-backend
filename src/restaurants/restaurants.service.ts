import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { Restaurants } from '../entity/restaurants.entity';
import { Comments } from 'src/entity/comments.entity';

// Get http://localhost:8000/restaurants
// Get http://localhost:8000/restaurants/${id}
// Post http://localhost:8000/restaurants
// Put http://localhost:8000/restaurants/${id}
// Delete http://localhost:8000/restaurants/${id}
// Get http://localhost:8000/restaurants/${id}/comments

@Injectable()
export class RestaurantsService {
  constructor(
    @InjectRepository(Restaurants) private restaurantRepo: Repository<Restaurants>,
    @InjectRepository(Comments) private commentRepo: Repository<Comments>,
  ) {}

  async findAll(): Promise<Restaurants[]> {
    return await this.restaurantRepo.find();
  }

  async findOne(id: number): Promise<Restaurants> {
    try {
      return await this.restaurantRepo.findOneBy({ id });
    } catch (error) {
      throw new Error(error);
    }
  }

  async create(
    name: string,
    location: string,
    avgRating: number,
  ): Promise<Restaurants> {
    try {
      const newRestaurant = new Restaurants();
      newRestaurant.name = name;
      newRestaurant.location = location;
      newRestaurant.avgRating = avgRating;
      return await this.restaurantRepo.save(newRestaurant);
    } catch (error) {
      throw new Error(error);
    }
  }

  async update(
    id: number,
    name: string,
    location: string,
    avgRating: number,
  ): Promise<Restaurants> {
    try {
      const restaurant = await this.restaurantRepo.findOneBy({ id });
      restaurant.name = name;
      restaurant.location = location;
      restaurant.avgRating = avgRating;
      return await this.restaurantRepo.save(restaurant);
    } catch (error) {
      throw new Error(error);
    }
  }

  async delete(id: number): Promise<Restaurants> {
    try {
      const restaurant = await this.restaurantRepo.findOneBy({ id });
      return await this.restaurantRepo.remove(restaurant);
    } catch (error) {
      throw new Error(error);
    }
  }

  async findComments(id: number): Promise<Comments[]> {
    try {
      return await this.commentRepo.find({ where: { restaurantId: id } });
    } catch (error) {
      throw new Error(error);
    }
  }
}
