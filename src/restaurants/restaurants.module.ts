import { Module } from '@nestjs/common';
import { RestaurantsController } from './restaurants.controller';
import { RestaurantsService } from './restaurants.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Restaurants } from '../entity/restaurants.entity';
import { Comments } from 'src/entity/comments.entity';

@Module({
  imports: [TypeOrmModule.forFeature([Restaurants, Comments])],
  controllers: [RestaurantsController],
  providers: [RestaurantsService],
})
export class RestaurantsModule {}
