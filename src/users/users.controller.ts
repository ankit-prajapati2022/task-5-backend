import { Controller, Delete, Get, Patch, Body, Param } from '@nestjs/common';
import { UsersService } from './users.service';
import { Users } from '../entity/users.entity';

// Get http://localhost:8000/users
// Delete http://localhost:8000/users/${editUser.id}
// Patch http://localhost:8000/users/${editUser.id}

@Controller('users')
export class UsersController {
  constructor(private readonly userService: UsersService) {}

  @Get()
  async findAll(): Promise<Users[]> {
    return await this.userService.findAll();
  }

  @Delete(':id')
  async delete(@Param('id') id: number): Promise<any> {
    return await this.userService.delete(id);
  }

  @Patch(':id')
  async update(
    @Param('id') id: number,
    @Body('name') name: string,
    @Body('email') email: string,
    @Body('role') role: string,
  ): Promise<Users> {
    return await this.userService.update(id, name, email, role);
  }
}
