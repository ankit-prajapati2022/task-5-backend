import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { Users } from '../entity/users.entity';

// Get http://localhost:8000/users
// Delete http://localhost:8000/users/${editUser.id}
// Patch http://localhost:8000/users/${editUser.id}

@Injectable()
export class UsersService {
  constructor(
    @InjectRepository(Users) private usersRepository: Repository<Users>,
  ) {}

  async findAll(): Promise<Users[]> {
    return await this.usersRepository.find();
  }

  async delete(id: number): Promise<any> {
    return await this.usersRepository.delete(id);
  }

  async update(
    id: number,
    name: string,
    email: string,
    role: string,
  ): Promise<Users> {
    const user = await this.usersRepository.findOneBy({ id });
    user.name = name;
    user.email = email;
    user.role = role;
    return await this.usersRepository.save(user);
  }
}
