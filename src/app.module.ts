import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { RestaurantsModule } from './restaurants/restaurants.module';
import { CommentsModule } from './comments/comments.module';
import { UsersModule } from './users/users.module';
import { TypeOrmModule } from '@nestjs/typeorm';
import { mySQLConfig } from './config/mysql.config';
import { AuthsModule } from './auths/auths.module';

@Module({
  imports: [
    RestaurantsModule,
    CommentsModule,
    UsersModule,
    AuthsModule,
    TypeOrmModule.forRoot(mySQLConfig),
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
