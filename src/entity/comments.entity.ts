import { Entity, PrimaryGeneratedColumn, Column } from 'typeorm';

@Entity('comments')
export class Comments {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  comment: string;

  @Column()
  avgRating: number;

  @Column()
  restaurantId: number;

  @Column()
  userId: number;

  @Column()
  date: string;
}
