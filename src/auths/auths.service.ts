import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { Users } from '../entity/users.entity';

@Injectable()
export class AuthsService {
  constructor(
    @InjectRepository(Users) private usersRepository: Repository<Users>,
  ) {}

  async login(
    email: string,
    password: string,
  ): Promise<{ user: Users; accessToken: string }> {
    const user = await this.usersRepository.findOneBy({ email });
    if (!user) {
      throw new Error('User not found');
    }
    if (user.password !== password) {
      throw new Error('Wrong password');
    }
    return { user: user, accessToken: '' };
  }

  async register(
    name: string,
    email: string,
    password: string,
    role: string,
  ): Promise<{ user: Users; accessToken: string }> {
    const user = new Users();
    user.name = name;
    user.email = email;
    user.password = password;
    user.role = role;
    return { user: await this.usersRepository.save(user), accessToken: '' };
  }
}
