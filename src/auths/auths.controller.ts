import { Controller, Post, Body } from '@nestjs/common';
import { AuthsService } from './auths.service';
import { Users } from '../entity/users.entity';

@Controller()
export class AuthsController {
  constructor(private readonly authsService: AuthsService) {}

  @Post('register')
  async register(
    @Body('name') name: string,
    @Body('email') email: string,
    @Body('password') password: string,
    @Body('role') role: string,
  ): Promise<{ user: Users; accessToken: string }> {
    return this.authsService.register(name, email, password, role);
  }

  @Post('login')
  async login(
    @Body('email') email: string,
    @Body('password') password: string,
  ): Promise<{ user: Users; accessToken: string }> {
    return this.authsService.login(email, password);
  }
}
