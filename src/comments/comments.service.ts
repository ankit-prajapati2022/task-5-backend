import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { Comments } from '../entity/comments.entity';

// Patch http://localhost:8000/comments/${editReview.id}
// Delete http://localhost:8000/comments/${editReview.id}
// Post http://localhost:8000/comments

@Injectable()
export class CommentsService {
  constructor(
    @InjectRepository(Comments) private commentsRepo: Repository<Comments>,
  ) {}

  async update(id: number, avgRating: number, comment: string, date: string) {
    const comments = await this.commentsRepo.findOneBy({ id });
    comments.avgRating = avgRating;
    comments.comment = comment;
    comments.date = date;
    const newComments = await this.commentsRepo.save(comments);
    return newComments;
  }

  async delete(id: number) {
    const comments = await this.commentsRepo.findOneBy({ id });
    await this.commentsRepo.remove(comments);
  }

    async create(avgRating: number, comment: string, date: string, restaurantId: number, userId: number) {
    const comments = new Comments();
    comments.avgRating = avgRating;
    comments.comment = comment;
    comments.date = date;
    comments.restaurantId = restaurantId;
    comments.userId = userId;
    const newComments = await this.commentsRepo.save(comments);
    return newComments;
  }
}
