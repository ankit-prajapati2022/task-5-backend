import { Controller, Delete, Patch, Post, Body, Param } from '@nestjs/common';
import { CommentsService } from './comments.service';
import { Comments } from '../entity/comments.entity';

// Patch http://localhost:8000/comments/${editReview.id}
// Delete http://localhost:8000/comments/${editReview.id}
// Post http://localhost:8000/comments

@Controller('comments')
export class CommentsController {
  constructor(private readonly commentService: CommentsService) {}

  @Patch(':id')
  async update(
    @Param('id') id: number,
    @Body('avgRating') avgRating: number,
    @Body('comment') comment: string,
    @Body('date') date: string,
  ) {
    await this.commentService.update(id, avgRating, comment, date);
  }

  @Delete(':id')
  async delete(@Param('id') id: number) {
    await this.commentService.delete(id);
  }

  @Post()
  async create(
    @Body('avgRating') avgRating: number,
    @Body('comment') comment: string,
    @Body('date') date: string,
    @Body('restaurantId') restaurantId: number,
    @Body('userId') userId: number,
  ) {
    await this.commentService.create(
      avgRating,
      comment,
      date,
      restaurantId,
      userId,
    );
  }
}
