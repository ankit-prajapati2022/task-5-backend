import { TypeOrmModuleOptions } from '@nestjs/typeorm';
import { Comments } from '../entity/comments.entity';
import { Restaurants } from '../entity/restaurants.entity';
import { Users } from '../entity/users.entity';

export const mySQLConfig: TypeOrmModuleOptions = {
  type: 'mysql',
  host: 'localhost',
  port: 3306,
  username: 'root',
  password: 'password',
  database: 'bitcs',
  entities: [Comments, Restaurants, Users],
  synchronize: true,
  logging: false,
  migrations: [],
  subscribers: [],
};
